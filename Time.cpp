#include <iostream>
using std::cout;

#include <iomanip>
using std::setfill;
using std::setw;

#include "Time.h"

Time::Time(int hr, int min, int sec) {
    setTime(hr,min,sec);
}

void Time::setTime(int h, int m, int s) {
    /*
    hour = ( h >= 0 && h < 24) ? h : 0;
    minute = (m >= 0 && m < 60) ? m : 0;
    second = ( s >= 0 && s < 60) ? s : 0;
    */
    setHour(h);
    setMinute(m);
    setSecond(s);
}


void Time::setHour(int h){
    hour = ( h >= 0 && h < 24) ? h : 0;
}

void Time::setMinute(int m) {
    minute = (m >= 0 && m < 60) ? m : 0;
}

void Time::setSecond(int s) {
    second = ( s >= 0 && s < 60) ? s : 0;
}
int Time::getHour() const{
    return hour;
}

/*
int &Time::badSetHour(int hh) {
    hour = (hh >= 0 && hh < 24) ? hh : 0;
    return hour;
}
*/


int Time::getMinute() const{
    return minute;
}

int Time::getSecond() const {
    return second;
}

void Time::printUniversal() const {
    cout << setfill('0') << setw(2) << getHour() << ":" << setw(2) << getMinute() << ":" << setw(2) << getSecond();
}

void Time::printStandart() const {
    cout << ((getHour() == 0 || getHour() == 12)? 12 : getHour() % 12) << ":"
    << setfill('0') << setw(2) << getMinute() << ":" << setw(2) << getSecond() << (getHour() < 12 ? "AM" : "PM");
}