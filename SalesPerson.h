//
// Created by JohnDoe on 04.04.2015.
//

#ifndef _CPP_DEITEL_SALESPERSON_H_
#define _CPP_DEITEL_SALESPERSON_H_

#include <math.h>

class SalesPerson
{
public:
    SalesPerson();
    void getSalesFromUser();
    void setSales(int, double);
    void printAnnualSales();

private:
    double totalAnnualSales();
    double sales[12];
};


#endif //_CPP_DEITEL_SALESPERSON_H_
