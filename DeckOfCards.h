//
// Created by JohnDoe on 27.03.2015.
//

#ifndef _CPP_DEITEL_DECKOFCARDS_H_
#define _CPP_DEITEL_DECKOFCARDS_H_

class DeckOfCards
{
public:
    DeckOfCards();
    void shuffle();
    void deal();

private:
    int deck[4][13];
};

#endif //_CPP_DEITEL_DECKOFCARDS_H_
