#include <string>
using std::string;

#ifndef _CPP_DEITEL_CREATEANDDESTROY_H_
#define _CPP_DEITEL_CREATEANDDESTROY_H_

class CreateAndDestroy
{
public:
    CreateAndDestroy(int, string);
    ~CreateAndDestroy();
private:
    int objectID;
    string message;
};

#endif //_CPP_DEITEL_CREATEANDDESTROY_H_