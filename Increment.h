#ifndef _CPP_DEITEL_INCREMENT_H_
#define _CPP_DEITEL_INCREMENT_H_

class Increment
{
public:
    Increment(int c = 0, int i = 1);
    void addIncrement()
    {
        count += increment;
    }
    void print() const;
private:
    int count;
    const int increment;
};

#endif //_CPP_DEITEL_INCREMENT_H_