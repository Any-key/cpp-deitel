#ifndef _CPP_DEITEL_DATE_H_
#define _CPP_DEITEL_DATE_H_

class Date
{
public:
    Date(int = 1, int = 1, int = 1900);
    void print() const;
    ~Date();
private:
    int month;
    int day;
    int year;

    int checkDay(int) const;
};

#endif //_CPP_DEITEL_DATE_H_