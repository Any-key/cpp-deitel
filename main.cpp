#include <iostream>
using std::cout;
using std::endl;

#include "Increment.h"

int main(){
    Increment value(10,5);
    cout << "Before:";
    value.print();

    for (int j = 10; j <= 3; j++)
    {
        value.addIncrement();
        cout << "After " << j << ":";
        value.print();
    }

    return 0;
}

/*
int main()
{
    char sentence[] = "This is a sentence with 7 tokens";
    char *tokenPtr;

    cout << "The string to be tokenized is :\n" << sentence << "\n\n The tokens are:\n\n";

    tokenPtr = strtok(sentence, " ");

    while (tokenPtr != NULL)
    {
        cout << tokenPtr << '\n';
        tokenPtr = strtok(NULL, " ");
    }

    cout << "\nAfter strtok, sentence = " << sentence << endl;
    return 0;
}
*/

/*
void selectionSort(int[], const int, bool (*)(int,int));
void swap(int * const, int * const);
bool ascending(int, int);
bool descending(int, int);

int main()
{
    const int arraySize = 10;
    int order;
    int counter;
    int a[arraySize] = {2, 6, 4, 8, 10, 12, 89, 68, 45, 37};

    cout << "1 - ascending\n2 - descending";
    cin >> order;
    cout << "\ndata in origin:\n";

    for (counter = 0; counter < arraySize; counter++)
        cout << setw(4) << a[counter];

    if (order == 1)
    {
        selectionSort(a,arraySize,ascending);
        cout << "\nData in ascending\n";
    }
    else
    {
        selectionSort(a,arraySize,descending);
        cout << "\nData in descending\n";
    }

    for (counter = 0; counter < arraySize; counter++)
        cout << setw(4) << a[counter];

    cout << endl;
    return 0;
}

void selectionSort(int work[], const int size, bool(*compare)(int,int))
{
    int smallOrLarge;

    for (int i = 0; i < size - 1; i++)
    {
        smallOrLarge = i;
        for (int index = i + 1; index < size; index++)
            if ((*compare)(work[index],work[smallOrLarge]))
                smallOrLarge = index;

        swap(&work[i], &work[smallOrLarge]);
    }
}

void swap(int * const el1Ptr, int *const el2Ptr)
{
    int hold = *el1Ptr;
    *el1Ptr = *el2Ptr;
    *el2Ptr = hold;
}

bool ascending(int a, int b)
{
    return a < b;
}

bool descending(int  a, int b)
{
    return a > b;
}
*/

/*

void selectionSort(int * const array, const int size)
{
    int smallest;
    for (int i = 0; i < size -1; i++)
    {
        smallest = i;
        for (int index = i+ 1; index < size; index++)
            if (array[index] < array[smallest])
                smallest = index;

        swap(&array[i],&array[smallest]);
    }
}

void swap (int * const element1Ptr, int * const element2Ptr)
{
    int hold = *element1Ptr;
    *element1Ptr = *element2Ptr;
    *element2Ptr = hold;
}

*/

/*
int main()
{
    vector<int> integers1(7);
    vector<int> integers2(10);

    cout << "Size of vector int1: " << integers1.size() << "\naafter init:" << endl;
    outputVector(integers1);

    cout << "Size of vector int2: " << integers2.size() << "\naafter init:" << endl;
    outputVector(integers2);

    cout << "\nEnter 17 int's:" << endl;
    inputVector(integers1);
    inputVector(integers2);

    cout << "\nAfter input int1:\n" << endl;
    outputVector(integers1);
    cout << "\nAfter input int2:\n" << endl;
    outputVector(integers2);

    vector<int> integers3(integers1);

    cout << "\nSize of int3:" << integers3.size() << "\nafter init:" << endl;
    outputVector(integers3);

    cout << "assign int2 to int1:" << endl;
    integers1 = integers2;

    cout << "\nInt1:\n" << endl;
    outputVector(integers1);
    cout << "\nInt2:\n" << endl;
    outputVector(integers2);

    cout << "\nEval: int1 == int2" << endl;

    if (integers1==integers2)
        cout << "int1 equal int2" << endl;

    cout << "\nint[5] is " << integers1[5];

    cout << "Assign 1000 to int1[5]:" << endl;
    integers1[5] = 1000;
    cout << "int1:" << endl;
    outputVector(integers1);

    cout << "\nAssign 1000 to int1[15]" << endl;
    integers1.at(15)=1000;

    return 0;
}

void outputVector(const vector<int> &array) {
    size_t i;
    for (i = 0; i < array.size(); i++) {
        cout << setw(12) << array[i];
        if (0 == (i + 1) % 4)
            cout << endl;
    }
}

void inputVector(vector<int> &array)
{
    for (size_t i = 0; i <array.size(); i++)
        cin >> array[i];
}
*/

/*
void modArray(int [], int);
void modEl(int);

void modArray(int b[], int sizeOfArray)
{
    for (int k = 0; k < sizeOfArray; k++)
        b[k] *= 2;
}
*/

/*
void modEl(int e)
{
    cout << "Value of el-t in modify: " << (e *= 2) << endl;
}
*/

/*
void printArray(const int[][3]);

int main() {
    int array1[2][3] = {{1, 2, 3}, {4, 5, 6}};
    int array2[2][3] = {1, 2, 3, 4, 5};
    int array3[2][3] = {{1, 2}, {4}};
    cout << "values in 1 be row:" << endl;
    printArray(array1);
    cout << "values in 2 be row:" << endl;
    printArray(array2);
    cout << "values in 3 be row:" << endl;
    printArray(array3);
    return 0;
}

void printArray(const int a[][3])
{
    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 3 ; j++)
            cout << a[i][j] << ' ';

    cout << endl;
}
*/

    /*
    * int gradesArray[ GradeBook::students] = {87, 68, 94, 100, 83, 78, 85, 91, 76, 87};

    GradeBook myGradeBook("CS101 intro in c++", gradesArray);
    myGradeBook.displayMessage();
    myGradeBook.processGrades();*/

    /*
    const int arraySize = 5;
    int a[arraySize] = {0, 1, 2, 3, 4};
    cout << "effects by reference: \n\nOriginal array:";
    for (int i = 0; i < arraySize; i++)
        cout << setw(3) << a[i];

    cout << endl;

    modArray(a, arraySize);
    cout << "modified array:\n";

    for (int j = 0; j < arraySize; j++)
        cout << setw(3) << a[j];

    cout << "\n\n\nPassing by value:" << "\n\na[3] before:" << a[3] << endl;
    modEl(a[3]);
    cout << "\n\n\nPassing by value:" << "\n\na[3] after:" << a[3] << endl;
    */

    /*
    char string1[20];
    char string2[] = "string literal";

    cout << "Enter the string \"hello there\": ";
    cin >> string1;

    cout << "string1 is: " << string1 << "\nstring2 is: " << string2;

    cout << "\nstring1 with spaces between characters is:\n";

    for (int i = 0; string1[i] != '\0'; i++)
        cout << string1[i] << ' ';

    cin >> string1;
    cout << "\nstring1 is: " << string1 << endl;

    for (int count = 0; count <= 10; count++)
        cout << setw(2) << count << "! = " << factor(count) << endl;
    */

/*
unsigned long factor(unsigned long num)
{
    unsigned long result = 1;
    for (unsigned long i = num; i >= 1; i--)
        result *= i;

}
*/

    /*
    int int1, int2, int3;

    cout << "Input three integer values: ";
    cin >> int1 >> int2 >> int3;
    cout << " the max integer is: " << maximum( int1, int2, int3);

    double double1, double2, double3;
    cout << "\n\nInput three double values: ";
    cin >> double1 >> double2 >> double3;

    cout << "The max double value is: " << maximum( double1, double2, double3 );

    char char1, char2, char3;
    cout << "\n\nInput three characters: ";
    cin >> char1 >> char2 >> char3;
    cout << "The max char value is: " << maximum( char1, char2, char3 );
    */

    /*
    cout << "local x in main's outer scope is " << x << endl;
    {
        int x = 7;
        cout << "local x in main's inner scope is " << x << endl;
    }

    cout << "local x in main's outer scope is " << x << endl;

    useLocal();
    useStaticLocal();
    useGlobal();
    useLocal();
    useStaticLocal();
    useGlobal();
    cout << "\nlocal x in main is " << x << endl;
    */

    /*
void useLocal( void )
{
    int x = 25;
    cout << "\nlocal x is " << x << " on entering useLocal" << endl;
    x++;
    cout << "local x is " << x << " on entering useLocal" << endl;
}

void useStaticLocal( void )
{
    static int x = 50;
    cout << "\nlocal static x is " << x <<
    useStaticLocal << " on entering" << endl;
    x++;
    cout << "local static x is " << x << " on existing useStaticLocal" << endl;
}

void useGlobal( void )
{
    cout << "\nglobal x is " << x << " on entering useGlobal" << endl;
    x *= 10;
    cout << "global x is " << x << " on existing useGlobal" << endl;
}

    */
    /*

    передача параметра по ссылке int &count - меньше затрат на копирование, но риск повреждения данных

    unsigned seed;
    cout << "Enter seed: ";
    cin >> seed;
    srand( time(0));
    for (int counter = 1; counter <= 10; counter++)
    {
        cout << setw(10) << ( 1 + rand() % 6);
        if (counter % 5 ==0)
            cout << endl;
    }
    */

    /*
    int freq1 = 0;
    int freq2 = 0;
    int freq3 = 0;
    int freq4 = 0;
    int freq5 = 0;
    int freq6 = 0;
    int face;

    for (int roll = 1; roll <= 6000000; roll++)
    {
        face = 1 + rand() % 6;
        switch ( face )
        {
            case 1:
                ++freq1;
                break;
            case 2:
                ++freq2;
                break;
            case 3:
                ++freq3;
                break;
            case 4:
                ++freq4;
                break;
            case 5:
                ++freq5;
                break;
            case 6:
                ++freq6;
                break;
            default:
                cout << "ERROR";
        }
    }

    cout << "Face" << setw(13) << "Frequency" << endl;
    cout << " 1" << setw(13) << freq1
            << "\n 2" << setw(13) << freq2
            << "\n 3" << setw(13) << freq3
            << "\n 4" << setw(13) << freq4
            << "\n 5" << setw(13) << freq5
            << "\n 6" << setw(13) << freq6 << endl;
    */

    /*
    GradeBook myGradeBook ("CS101 Intro to C++ Programming");
    myGradeBook.displayMessage();
    myGradeBook.inputGrades();
    myGradeBook.displayGradeReport();
    */

    /*
    for (int i= 1; i <=10; i++)
    {
        if (5 == i)
            continue;
        cout << i <<"\n";
    }
    cout << endl;
    */

    /*
    ������������� ������ �������
    (1>0) ? cout << "\nTrue"  : cout << "\nFalse" );
    */


