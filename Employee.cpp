#include <iostream>
using std::cout;
using std::endl;

#include <cstring>
using std::strlen;
using std::ctrncpy;

#include "Employee.h"
#include "Date.h"

Employee::Employee(char const *const first, char const *const last, Date const &dateOfBirth, Date const &DateOfHire)
        : birthDate(dateOfBirth), hireDate(dateOfHire)
{
    int length = strlen (first);
    length = (length < 25 ? length : 24);
    strncpy(firstName, first, length);
    firstName[length] = '\0';

    length = strlen(last);
    length = (length < 25 ? length : 24);
    strncpy(lastName, first, length);
    lastName[length] = '\0';

    cout << "Employee construct: " << firstName << " " << lastName << endl;
}

void Employee::print() const
{
    cout << lastName << ", " << firstName << " hired";
    hireDate.print();
    cout << " birthday: ";
    birthDate.print();
    cout << endl;
}

Employee::~Employee()
{
    cout << "Employee destructor: " << lastName << ", " << firstName << endl;
}